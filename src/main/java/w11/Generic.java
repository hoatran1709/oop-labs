package w11;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Generic
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class Generic {
    /**
     * Generic sort list
     *
     * @param list list to sort
     * @return sorted list
     */
    public static <T extends Comparable<? super T>> T[] sort(T[] list) {
        List<T> toList = Arrays.asList(list);
        Collections.sort(toList);

        return toList.toArray(list);
    }

    /**
     * Generic find max element of a list
     *
     * @param list list to find
     * @return the max element
     */
    public static <T extends Comparable<? super T>> T maxElement(T[] list) {
        return Collections.max(Arrays.asList(list));
    }

    /**
     * Main function
     *
     * @param args list of parameters for main
     */
    public static void main(String[] args) {
        //////// Sort ///////
        System.out.println("-----------------------------------------------------");

        String[] arrString = sort(new String[]{"b", "B", "a", "C", "z"});
        System.out.println(Arrays.asList(arrString));

        Integer[] arrInteger = sort(new Integer[]{1, 2, -33, 10, 9, 2, 4});
        System.out.println(Arrays.asList(arrInteger));

        Double[] arrDouble = sort(new Double[]{-100.0, -200.0, 3.0, 1.5, 2.0});
        System.out.println(Arrays.asList(arrDouble));

        System.out.println("-----------------------------------------------------");

        ///////// Find max /////////
        System.out.println("-----------------------------------------------------");

        System.out.println("Max of current list is : " + maxElement(new String[]{"b", "B", "a", "C", "z"}));
        System.out.println("Max of current list is : " + maxElement(new Integer[]{1, 2, -33, 10, 9, 2, 4}));
        System.out.println("Max of current list is : " + maxElement(new Double[]{-100.0, -200.0, 3.0, 1.5, 2.0}));

        System.out.println("-----------------------------------------------------");
    }
}
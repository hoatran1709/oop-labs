package w12.ex1;

import java.util.List;

/**
 * Main class for week 12 exercise 1
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class Main {

    /**
     * Main function
     *
     * @param args list parameters
     */
    public static void main(String[] args) {
        Person James = new Person("James", "20/03/1976", "Male", true, null);
        Person Hana = new Person("Hana", "01/06/1980", "Female", true, James);
        James.setPartner(Hana);

        Person Ryan = new Person("Ryan", "06/10/2000", "Male", false, null);

        Person Kai = new Person("Kai", "10/11/2005", "Male", true, null);
        Person Jennifer = new Person("Jennifer", "12/03/2010", "Female", true, Kai);
        Kai.setPartner(Jennifer);

        Person Bob = new Person("Bob", "01/01/2030", "Male", false, null);
        Person John = new Person("John", "19/12/2030", "Male", true, null);
        Person Oriana = new Person("Oriana", "08/01/2034", "Female", false, null);
        Person Anna = new Person("Anna", "30/05/2036", "Female", true, null);

        James.addAllChildren(Ryan, Kai);
        Hana.syncChildrenWithPartner();

        Kai.addAllChildren(Bob, John, Oriana, Anna);
        Jennifer.syncChildrenWithPartner();

        Family family = new Family();

        family.setAncestor(James);

        System.out.println(family.toString());

        System.out.println("\n---------------- ALL PEOPLE WHO ARE NOT MARRIED ----------------\n");
        List<Person> whoAreNotMarried = family.getAllSinglePeopleInFamily();

        for (Person person : whoAreNotMarried) System.out.println("\t\t" + person.getName());

        System.out.println("\n----------------------------------------------------------------\n");

        System.out.println("\n-------------- ALL COUPLE WHO ARE HAVE TWO CHILDREN --------------\n");
        List<Person> whoHaveTwoChildren = family.getAllCoupleHaveTwoChildren();

        for (Person person : whoHaveTwoChildren)
            System.out.println("\t\t" + person.getName() + " and " + person.getPartner().getName());

        System.out.println("\n------------------------------------------------------------------\n");

        System.out.println("\n----------------- NEWEST GENERATION OF THE FAMILY -----------------\n");
        List<Person> newestGeneration = family.getNewestGeneration();

        for (Person person : newestGeneration) System.out.println("\t\t" + person.getName());

        System.out.println("\n-------------------------------------------------------------------\n");
    }
}
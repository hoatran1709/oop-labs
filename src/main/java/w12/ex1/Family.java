package w12.ex1;

import java.util.ArrayList;
import java.util.List;

/**
 * Family
 */
public class Family {
    private Person ancestor;
    public static int currentDeepDistanceOfTree = 0;

    /**
     * Constructor
     */
    public Family() {
        ancestor = null;
    }

    /**
     * @param ancestor the ancestor to set
     */
    public void setAncestor(Person ancestor) {
        this.ancestor = ancestor;
        this.ancestor.setDistanceToAncestors(0);
    }

    /**
     * @return the ancestor
     */
    public Person getAncestor() {
        return ancestor;
    }

    /**
     * Family infomation to string
     */
    @Override
    public String toString() {
        StringBuffer result = new StringBuffer();

        result.append("\n----------------------- FAMILY TREE VIEW -----------------------\n");

        if (ancestor != null)
            result.append(ancestor.toString());

        result.append("----------------------------------------------------------------\n");

        return result.toString();
    }

    /**
     * Find all people who are not married
     *
     * @return a list of people
     */
    public List<Person> getAllSinglePeopleInFamily() {
        List<Person> result = new ArrayList<Person>();

        result = ancestor.getSingleChildren(result);

        return result;
    }

    /**
     * Find all couple who have two children
     *
     * @return list of person
     */
    public List<Person> getAllCoupleHaveTwoChildren() {
        List<Person> result = new ArrayList<Person>();

        result = ancestor.getChildrenHaveTwoChildren(result);

        return result;
    }

    /**
     * Find newest generation of family
     *
     * @return list or person
     */
    public List<Person> getNewestGeneration() {
        List<Person> result = new ArrayList<Person>();

        result = ancestor.getNewestChildren(result);

        return result;
    }
}
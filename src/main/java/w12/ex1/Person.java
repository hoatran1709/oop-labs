package w12.ex1;

import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Person
 */
public class Person {
    private String birthday;
    private String name;
    private String gender;
    private boolean isMarried;
    private Person partner;
    private Pair<Person, Person> parent;
    private List<Person> children;
    private int distanceToAncestors;

    public Person() {
        birthday = name = gender = "";
        isMarried = false;
        partner = null;
        parent = null;
        distanceToAncestors = 0;
        children = new ArrayList<Person>();

        try {
            // ... do something
        } catch (NullPointerException npe) {
            // ... Null object reached
        }

        Integer object = null;

        if (object != null) {
            // ... do something
        } else {
            // ... oh, do something diffirent things
        }
    }

    public Person(String name, String birthday, String gender, boolean isMarried, Person partner) {
        this();
        children = new ArrayList<Person>();
        this.name = name;
        this.birthday = birthday;
        this.gender = gender;
        this.isMarried = isMarried;
        this.partner = partner;
    }

    /**
     * @return the children
     */
    public List<Person> getChildren() {
        return children;
    }

    /**
     * @param children the children to set
     */
    public void setchildren(List<Person> children) {
        this.children = children;
    }

    /**
     * @param parent the parent to set
     */
    public void setParent(Pair<Person, Person> parent) {
        this.parent = parent;
    }

    /**
     * @return the parent
     */
    public Pair<Person, Person> getParent() {
        return parent;
    }

    /**
     * @return the distanceToAncestors
     */
    public int getDistanceToAncestors() {
        return distanceToAncestors;
    }

    /**
     * @param distanceToAncestors the distanceToAncestors to set
     */
    public void setDistanceToAncestors(int distanceToAncestors) {
        this.distanceToAncestors = distanceToAncestors;
    }

    /**
     * Add a child
     *
     * @param child child to add
     * @return add status
     */
    public boolean addChild(Person child) {
        if (this.children.add(child)) {
            child.setParent(new Pair<Person, Person>(this, this.partner));
            child.distanceToAncestors = this.distanceToAncestors + 1;

            Family.currentDeepDistanceOfTree = Math.max(child.distanceToAncestors, Family.currentDeepDistanceOfTree);

            return true;
        }
        return false;
    }

    /**
     * Add multi children
     *
     * @param children children to add
     * @return add status
     */
    public boolean addAllChildren(Person... children) {
        for (Person child : children) {
            if (!addChild(child))
                return false;
        }

        return true;
    }

    /**
     * Remove a child
     *
     * @param child child to remove
     * @return remove status
     */
    public boolean removeChild(Person child) {
        if (this.children.remove(child) && child.partner != null && this.children.remove(child.partner))
            return true;
        else
            return false;
    }

    /**
     * @return the isMarried
     */
    public boolean isMarried() {
        return isMarried;
    }

    /**
     * @param isMarried the isMarried to set
     */
    public void setIsMarried(boolean isMarried) {
        this.isMarried = isMarried;
    }

    /**
     * @return the birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * @param birthday the birthday to set
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the partner
     */
    public Person getPartner() {
        return partner;
    }

    /**
     * @param partner the partner to set
     */
    public void setPartner(Person partner) {
        this.partner = partner;
        this.partner.distanceToAncestors = this.distanceToAncestors;
    }

    /**
     * Copy a children from the partner to this person's children
     */
    public void syncChildrenWithPartner() {
        this.children = this.partner.getChildren();

        Collections.copy(this.children, this.partner.getChildren());
    }

    @Override
    public String toString() {
        StringBuffer result = new StringBuffer();

        for (int tab = 0; tab < this.distanceToAncestors; ++tab)
            result.append("\t");
        result.append(this.name);

        if (isMarried && this.partner != null)
            result.append(" and " + this.partner.name + "\n");

        if (isHavingChildren())
            for (Person child : children)
                if (child != null)
                    result.append(child.toString() + "\n");

        return result.toString();
    }

    @Override
    public boolean equals(Object other) {
        return ((other instanceof Person) && (this.name.equalsIgnoreCase(((Person) other).name))
                && (this.birthday.equalsIgnoreCase(((Person) other).birthday))
                && (this.gender.equalsIgnoreCase(((Person) other).gender))
                && (this.isMarried == ((Person) other).isMarried));
    }

    /**
     * Find all children who are single
     *
     * @return a list of people
     */
    public List<Person> getSingleChildren(List<Person> result) {
        if (this.isMarried) {
            for (Person child : children)
                result = child.getSingleChildren(result);
        } else {
            if (!result.contains(this))
                result.add(this);
        }

        return result;
    }

    /**
     * Check if person has two children
     *
     * @return two children having status
     */
    public boolean isHavingTwoChildren() {
        return (this.children.size() == 2);
    }

    /**
     * Get all children have two children of this person
     *
     * @param result list result
     * @return a new result
     */
    public List<Person> getChildrenHaveTwoChildren(List<Person> result) {
        if (this.isMarried && !result.contains(this.partner) && !result.contains(this) && isHavingTwoChildren())
            result.add(this);

        for (Person child : children)
            result = child.getChildrenHaveTwoChildren(result);

        return result;
    }

    /**
     * Check if person has children
     *
     * @return child having status
     */
    public boolean isHavingChildren() {
        return (this.children.size() > 0);
    }

    /**
     * Get newest children of this person as family longest distance
     *
     * @param result result list of person
     * @return a new list
     */
    public List<Person> getNewestChildren(List<Person> result) {
        if (this.distanceToAncestors == Family.currentDeepDistanceOfTree && !result.contains(this))
            result.add(this);
        else {
            if (this.isHavingChildren())
                for (Person child : children)
                    result = child.getNewestChildren(result);
        }

        return result;
    }
}
package w12.ex2;

import java.util.Arrays;
import java.util.List;

/**
 * Main
 */
public class Main {
    public static void main(String[] args) {
        List<Integer> testList = Arrays.asList(-1, 2, 5, 10, 100, -10, 7, 9, 0, 2, 2, -134, 899);
        System.out.println("-------------------------------------------------------------------------");
        System.out.println("List to sort\t= " + testList);
        System.out.println("-------------------------------------------------------------------------");

        SortingAlgorithms.Sorter sorter = null;

        sorter = new SortingAlgorithms.BubbleSort<>();
        System.out.println("Bubble sort\t= " + sorter.sort(testList));

        sorter = new SortingAlgorithms.SelectionSort<>();
        System.out.println("Selection sort\t= " + sorter.sort(testList));

        sorter = new SortingAlgorithms.QuickSort<>();
        System.out.println("Quick sort\t= " + sorter.sort(testList));

        sorter = new SortingAlgorithms.MergeSort<>();
        System.out.println("Merge sort\t= " + sorter.sort(testList));

        sorter = new SortingAlgorithms.ShellSort<>();
        System.out.println("Shell sort\t= " + sorter.sort(testList));

        System.out.println("-------------------------------------------------------------------------");
    }
}
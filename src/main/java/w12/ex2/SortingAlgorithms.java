package w12.ex2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * SortingAlgorithms
 */
public class SortingAlgorithms {
    /**
     * Sorter interface
     *
     * @param <T> Number type
     */
    public static interface Sorter<T extends Number> {
        /**
         * Sort function where algorithm run
         *
         * @param list list to sort
         * @return list sorted
         */
        public List<T> sort(List<T> list);
    }

    /**
     * Bubble sort algorithm
     *
     * @param <T> Number type
     */
    public static class BubbleSort<T extends Number> implements Sorter<T> {
        @Override
        public List<T> sort(List<T> list) {
            for (int i = 0; i < list.size() - 1; ++i)
                for (int j = i; j < list.size(); ++j)
                    if (list.get(i).doubleValue() > list.get(j).doubleValue())
                        Collections.swap(list, i, j);

            return list;
        }
    }

    /**
     * SelectionSort sort algorithm
     *
     * @param <T> Number type
     */
    public static class SelectionSort<T extends Number> implements Sorter<T> {
        private int findCurrentMin(List<T> list, int start, int end) {
            int resultIndex = start;
            T resultMin = list.get(start);

            for (int i = start; i < end; ++i) {
                if (list.get(i).doubleValue() < resultMin.doubleValue()) {
                    resultIndex = i;
                    resultMin = list.get(i);
                }
            }

            return resultIndex;
        }

        @Override
        public List<T> sort(List<T> list) {
            for (int i = 0; i < list.size(); ++i) {
                int indexMin = findCurrentMin(list, i, list.size());
                Collections.swap(list, i, indexMin);
            }

            return list;
        }
    }

    /**
     * Quick sort algorithm
     *
     * @param <T> Number type
     */
    public static class QuickSort<T extends Number> implements Sorter<T> {
        private void doQuickSort(List<T> list, int left, int right) {
            if (list.isEmpty())
                return;

            if (left >= right)
                return;

            int middle = left + (right - left) / 2;
            int i = left, j = right;
            double pivot = list.get(middle).doubleValue();

            while (i <= j) {
                while (i <= j && list.get(i).doubleValue() < pivot)
                    i++;
                while (i <= j && list.get(j).doubleValue() > pivot)
                    j--;

                if (i <= j) {
                    Collections.swap(list, i, j);
                    i++;
                    j--;
                }
            }

            if (left < j)
                doQuickSort(list, left, j);
            if (right > i)
                doQuickSort(list, i, right);
        }

        @Override
        public List<T> sort(List<T> list) {
            doQuickSort(list, 0, list.size() - 1);

            return list;
        }
    }

    /**
     * Merge sort algorithm
     *
     * @param <T> Number type
     */
    public static class MergeSort<T extends Number> implements Sorter<T> {
        private void merge(List<T> list, int l, int m, int r) {
            int n1 = m - l + 1;
            int n2 = r - m;

            List<T> L = new ArrayList<>(n1);
            List<T> R = new ArrayList<>(n2);

            for (int i = 0; i < n1; i++)
                L.add(list.get(l + i));
            for (int j = 0; j < n2; j++)
                R.add(list.get(m + 1 + j));

            int i = 0, j = 0, k = l;

            while (i < n1 && j < n2) {
                if (L.get(i).doubleValue() <= R.get(j).doubleValue()) {
                    list.set(k, L.get(i));
                    i++;
                } else {
                    list.set(k, R.get(j));
                    j++;
                }
                k++;
            }

            while (i < n1) {
                list.set(k, L.get(i));
                i++;
                k++;
            }

            while (j < n2) {
                list.set(k, R.get(j));
                j++;
                k++;
            }
        }

        private void doMergeSort(List<T> list, int l, int r) {
            if (l < r) {
                int m = l + (r - l) / 2;

                doMergeSort(list, l, m);
                doMergeSort(list, m + 1, r);

                merge(list, l, m, r);
            }
        }

        @Override
        public List<T> sort(List<T> list) {
            doMergeSort(list, 0, list.size() - 1);

            return list;
        }
    }

    /**
     * Shell sort algorithm
     *
     * @param <T> Number type
     */
    public static class ShellSort<T extends Number> implements Sorter<T> {
        @Override
        public List<T> sort(List<T> list) {
            for (int gap = list.size() / 2; gap > 0; gap /= 2) {
                for (int i = gap; i < list.size(); i += 1) {
                    T temp = list.get(i);
                    int j;
                    for (j = i; j >= gap && list.get(j - gap).doubleValue() > temp.doubleValue(); j -= gap)
                        list.set(j, list.get(j - gap));

                    list.set(j, temp);
                }
            }
            return list;
        }
    }
}
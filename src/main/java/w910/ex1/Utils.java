package w910.ex1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utils - Main class for week 9 + 10 exercise 1
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class Utils {
    /**
     * Read content from a file
     *
     * @param path path to file
     * @return all content as a string in the file
     */
    public static String readContentFromFile(String path) {
        Scanner scan = null;

        try {
            scan = new Scanner(new File(path));

            StringBuilder res = new StringBuilder();

            while (scan.hasNextLine()) {
                res.append(scan.nextLine() + "\n");
            }

            return res.toString();
        } catch (FileNotFoundException fail) {
            fail.printStackTrace();
        } finally {
            scan.close();
        }

        return "";
    }

    /**
     * Write a string to a file
     *
     * @param path    path to destination
     * @param content content to write
     * @param append  is append
     */
    public static void writeContentToFile(String path, String content, boolean append) {
        PrintWriter writer = null;

        try {
            File file = new File(path);
            file.createNewFile();

            writer = new PrintWriter(new FileWriter(file, append));
            writer.println(content);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            writer.flush();
            writer.close();
        }
    }

    /*
     *
     * public static void FAIL(String path, String content, boolean append) {
     * PrintWriter writer = null;
     *
     * try { File file = new File(path); file.createNewFile();
     *
     * writer = new PrintWriter(new FileWriter(file, append));
     * writer.println(content); } catch (Exception e) { e.printStackTrace(); }
     * finally { writer.flush(); writer.close(); } }
     *
     */

    /**
     * Find file by name
     *
     * @param folderPath fodler path
     * @param fileName   file name in folder
     * @return File
     */
    public static File findFileByName(String folderPath, String fileName) {
        File file = new File(folderPath + "/" + fileName);

        if (file.exists())
            return file;
        else
            return null;
    }

    /**
     * Get all functions
     *
     * @param path path to file
     * @return list of string function
     */
    public static List<String> getAllFunctions(File path) {
        String data = readContentFromFile(path.getAbsolutePath());

        List<String> cutter = new ArrayList<>(Arrays.asList(data.split("\n")));
        List<String> res = new ArrayList<>();

        int currentLineIndex = 0;
        int isInComment = 0;

        for (String datas : cutter) {
            if (!data.trim().startsWith("*")) {

                if (datas.trim().startsWith("/*")) {
                    if (datas.contains("*/"))
                        datas = datas.substring(datas.lastIndexOf("*/") + 1);
                    else
                        isInComment++;
                }

                if (isInComment == 0) {
                    if (checkStringMatchRegex(datas,
                            "^[^(//)][\\s]*(?:(?:public|protected|private)\\s+)*(?:(static|abstract)\\s+)+[\\w\\<\\>\\[\\]]+\\s+(\\w+)*\\([^\\)]*\\)*(\\{?|[^;])")) {
                        datas = datas.trim();

                        if (!datas.endsWith(";")) {
                            List<String> belowFunction = cutter.subList(currentLineIndex, cutter.size() - 1);
                            res.add(findFullContentOfFunction(belowFunction));
                        } else
                            res.add(datas);
                    }
                } else {
                    if (datas.contains("*/"))
                        isInComment--;
                }
            }

            currentLineIndex++;
        }

        return res;
    }

    public static String findFullContentOfFunction(List<String> fileDataBelowFunctionName) {
        StringBuffer res = new StringBuffer();
        int count = 0;

        boolean gotFirst = false;

        for (int i = 0; i < fileDataBelowFunctionName.size(); ++i) {
            String currentLine = fileDataBelowFunctionName.get(i);

            for (int j = 0; j < currentLine.length(); ++j) {

                if (count <= 0 && gotFirst)
                    return res.toString();

                char currentChar = currentLine.charAt(j);

                res.append(currentChar);

                if (currentChar == '{') {
                    if (!gotFirst)
                        gotFirst = true;
                    count++;
                }
                if (currentChar == '}')
                    count--;
            }

            res.append("\n");
        }

        return res.toString();
    }

    /**
     * Find functoin by name
     *
     * @param name name of function
     * @param path path to file
     * @return full name of function in file
     */
    public static String findFunctionByName(String name, File path) {
        List<String> functions = getAllFunctions(path);

        for (String item : functions) {
            String cutFunctionName = item.substring(0, item.indexOf("{"));
            if (name.equalsIgnoreCase(changeFullFunctionToNameFormat(cutFunctionName)))
                return item;
        }

        return "Function does not exist!";
    }

    /**
     * Check string match a regex
     *
     * @param input input string
     * @param regex regex to use
     * @return true/false
     */
    private static boolean checkStringMatchRegex(String input, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);

        if (matcher.find())
            return true;

        return false;
    }

    /**
     * Convert full name to short name of a function : <Fucntion Name>(<TypeParam1>,
     * <TypeParam2>, ...)
     *
     * @param functionFull full name
     * @return short name
     */
    private static String changeFullFunctionToNameFormat(String functionFull) {
        functionFull = functionFull.trim();
        String res = "";

        int firstParenthese = functionFull.indexOf("("); // Get index of first ( which start of parameters of function

        String cutBefore = functionFull.substring(0, firstParenthese); // Get before first ( string
        int lastSpaceInCut = cutBefore.lastIndexOf(" "); // Get last index of whitespace before name of function
        String cutName = cutBefore.substring(lastSpaceInCut, cutBefore.length()); // Cut name of function from last
        // whitespace to first (
        res = cutName;

        String cutAfter = functionFull.substring(firstParenthese, functionFull.length() - 2).trim(); // Cut substring
        // after first ( to
        // end of full
        // function
        cutAfter = cutAfter.replaceAll("(\\s+)(\\w+)?([,)]+)", "$3"); // Replace all parameters in function to format :
        // (<Type>, <Type>, ...) keep the , or ) with $3

        return res.trim() + cutAfter; // Result is a string with the name of function and its type or parameters
    }

    /**
     * Main function
     *
     * @param args list of parameters
     */
    public static void main(String[] args) {
        System.out.println("----------------------------------------------------");
        List<String> functions = getAllFunctions(findFileByName("./", "Utils.java"));
        for (String function : functions)
            System.out.println(function + "\n");
        // System.out.println(functions.get(0));
        System.out.println("----------------------------------------------------");
    }
}
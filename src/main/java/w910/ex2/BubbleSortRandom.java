package w910.ex2;

import java.util.Collections;
import java.util.Random;
import java.util.Vector;

/**
 * BubbleSortRandom - Main class for week 10 exercise 2
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class BubbleSortRandom {

    /**
     * Generate double list
     *
     * @return result list
     */
    public static Vector<Double> generateDoubleList() {
        Vector<Double> res = new Vector<>();

        for (int i = 0; i < 10; ++i)
            res.add(new Random().nextDouble());

        return res;
    }

    /**
     * Bublesort
     *
     * @param list list to sort
     * @return result list
     */
    public static Vector<Double> bublesort(Vector<Double> list) {
        for (int i = 0; i < list.size() - 1; ++i)
            for (int j = i; j < list.size(); ++j)
                if (list.get(j).compareTo(list.get(i)) < 0)
                    Collections.swap(list, i, j);

        return list;
    }

    /**
     * Print list
     *
     * @param list list
     */
    public static void print(Vector<Double> list) {
        System.out.println();
        for (Double item : list) {
            System.out.printf("%.2f ", item.doubleValue());
        }
    }

    /**
     * Main function
     *
     * @param args list parameters
     */
    public static void main(String[] args) {
        Vector<Double> list = generateDoubleList();

        print(list);

        list = bublesort(list);

        print(list);
    }
}
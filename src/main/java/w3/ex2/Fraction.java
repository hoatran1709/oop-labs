package w3.ex2;

import w3.ex1.Main;

public class Fraction {
    private int n, d;

    public Fraction() {
    }

    public Fraction(int n, int d) {
        this.n = n;
        this.d = d;
    }

    public Fraction(Fraction other) {
        this.d = other.d;
        this.n = other.n;
    }

    /**
     * Create new Fraction from data of this Fraction
     *
     * @return new Fraction created
     */
    public Fraction clone() {
        return new Fraction(this);
    }

    /**
     * Minimal Fraction
     *
     * @return this Fraction after minimal
     */
    public Fraction min() {
        int nTemp = this.n, dTemp = this.d;

        this.n /= Main.GCD(nTemp, dTemp);
        this.d /= Main.GCD(nTemp, dTemp);

        return this;
    }

    /**
     * Add this Fraction with another
     *
     * @param other other Fraction
     * @return this Fraction after get result
     */
    public Fraction add(Fraction other) {
        this.n = this.n * other.d + this.d * other.n;
        this.d = this.d * other.d;

        min();

        return this;
    }

    /**
     * Subtract this Fraction with another
     *
     * @param other other Fraction
     * @return this Fraction after get result
     */
    public Fraction sub(Fraction other) {
        this.n = this.n * other.d - this.d * other.n;
        this.d = this.d * other.d;

        min();

        return this;
    }

    /**
     * Divide this Fraction with another
     *
     * @param other other Fraction
     * @return this Fraction after get result
     */
    public Fraction div(Fraction other) {
        this.n = this.n / other.n;
        this.d = this.d / other.d;

        min();

        return this;
    }

    /**
     * Multiple this Fraction with another
     *
     * @param other other Fraction
     * @return this Fraction after get result
     */
    public Fraction mul(Fraction other) {
        this.n = this.n * other.n;
        this.d = this.d * other.d;

        min();

        return this;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Fraction) {
            min();
            ((Fraction) obj).min();

            return (this.n == ((Fraction) obj).n && this.d == ((Fraction) obj).d);
        }

        return false;
    }

    @Override
    public String toString() {
        return this.n + " / " + this.d;
    }
}

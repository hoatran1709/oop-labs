package w3.ex2;

/**
 * Main class for week 3 exercise 2
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class Main {

    /**
     * Main function
     *
     * @param args list parameters
     */
    public static void main(String[] args) {
        Fraction f1 = new Fraction(8, 3);
        Fraction f2 = new Fraction(10, 4);

        System.out.println(f1.toString());
        System.out.println(f2.toString());

        System.out.println(f1.min());
        System.out.println(f2.min());

        System.out.println(f1.clone().add(f2));
        System.out.println(f1.clone().sub(f2));
        System.out.println(f1.clone().div(f2));
        System.out.println(f1.clone().mul(f2));
    }
}

package w3.ex1;

/**
 * Main class for week 3 exercise 1
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class Main {

    /**
     * Find greatest common division of two numbers
     *
     * @param a number one
     * @param b number two
     * @return the GCD
     */
    public static int GCD(int a, int b) {
        if (a == 0) return b;
        if (b == 0) return a;

        a = Math.abs(a);
        b = Math.abs(b);

        while (a != b) {
            if (a > b) a -= b;
            else b -= a;
        }

        return a;
    }

    /**
     * Get n-th fibonacci number
     *
     * @param n n-th
     * @return the n-th fibonacci number
     */
    private static int fibonacci(int n) {
        if (n <= 0) return 0;
        if (n == 1) return 1;

        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    /**
     * Main function
     *
     * @param args list parameters
     */
    public static void main(String[] args) {
        System.out.println(GCD(8, 6));
        System.out.println("-----------------------------------");
        System.out.println(fibonacci(5));
    }
}

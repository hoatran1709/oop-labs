package w3.ex3;

public class Human extends Entity {
    protected int salaray;
    protected String email;
    protected String address;

    public Human() {
    }

    public Human(int salaray, String email, String address) {
        this.salaray = salaray;
        this.email = email;
        this.address = address;
    }

    public Human(String name, int lifeSpan, float speed, int salaray, String email, String address) {
        super(name, lifeSpan, speed);
        this.salaray = salaray;
        this.email = email;
        this.address = address;

        System.out.println(toString());
    }

    public int getSalaray() {
        return salaray;
    }

    public void setSalaray(int salaray) {
        this.salaray = salaray;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("- %d$/month - %s - %s", salaray, email, address);
    }
}

package w3.ex3;

import w3.ex3.ten.*;

/**
 * Main class for week 3 exercise 3
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class Main {

    /**
     * Main function
     *
     * @param args list parameters
     */
    public static void main(String[] args) {
        Myself myself = new Myself();
        MyGirlFriend myGirlFriend = new MyGirlFriend();
        Teacher teacher = new Teacher();
        Employee employee = new Employee();
        Worker worker = new Worker();

        Tiger tiger = new Tiger();
        Wolf wolf = new Wolf();
        Rabbit rabbit = new Rabbit();
        Dog dog = new Dog();
        Cat cat = new Cat();
    }
}

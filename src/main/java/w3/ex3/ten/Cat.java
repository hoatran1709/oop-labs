package w3.ex3.ten;

import w3.ex3.Animal;

public class Cat extends Animal {

    public Cat() {
        super("Cat", 50, 120, "Family Animal", false);
    }
}

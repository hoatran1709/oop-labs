package w3.ex3.ten;

import w3.ex3.Animal;

public class Rabbit extends Animal {

    public Rabbit() {
        super("Rabbit", 60, 100, "Grass Eater Animal", false);
    }
}

package w3.ex3.ten;

import w3.ex3.Animal;

public class Dog extends Animal {

    public Dog() {
        super("Dog", 40, 120, "Family Animal", false);
    }
}

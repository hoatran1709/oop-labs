package w3.ex3.ten;

import w3.ex3.Animal;

public class Tiger extends Animal {

    public Tiger() {
        super("Tiger", 50, 120, "Hunter Animal", true);
    }
}

package w3.ex3.ten;

import w3.ex3.Animal;

public class Wolf extends Animal {

    public Wolf() {
        super("Wolf", 50, 120, "Hunter Animal", true);
    }
}

package w3.ex3.ten;

import w3.ex3.Human;

public class Worker extends Human {

    public Worker() {
        super("Worker", 80, 30, 1000, "Some Email", "Some address");
    }
}

package w3.ex3.ten;

import w3.ex3.Human;

public class Employee extends Human {

    public Employee() {
        super("Employee", 80, 30, 1000, "Some Email", "Some address");
    }
}

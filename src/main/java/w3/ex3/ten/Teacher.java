package w3.ex3.ten;

import w3.ex3.Human;

public class Teacher extends Human {

    public Teacher() {
        super("Teacher", 80, 30, 1000, "Some Email", "Some address");
    }
}

package w3.ex3;

public abstract class Entity {
    protected String name;
    protected int lifeSpan;
    protected float speed;

    public Entity() {
    }

    public Entity(String name, int lifeSpan, float speed) {
        this.name = name;
        this.lifeSpan = lifeSpan;
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(int lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        return String.format("%s : %d - %.2f ", name, lifeSpan, speed);
    }
}

package w3.ex3;

public class Animal extends Entity {

    protected String group;
    protected boolean isDangerous;

    public Animal() {
    }

    public Animal(String group, boolean isDangerous) {
        this.group = group;
        this.isDangerous = isDangerous;
    }

    public Animal(String name, int lifeSpan, float speed, String group, boolean isDangerous) {
        super(name, lifeSpan, speed);
        this.group = group;
        this.isDangerous = isDangerous;

        System.out.println(toString());
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public boolean isDangerous() {
        return isDangerous;
    }

    public void setDangerous(boolean dangerous) {
        isDangerous = dangerous;
    }

    @Override
    public String toString() {
        return super.toString() + String.format("- %s - %s", group, (isDangerous) ? "dangerous" : "familiar");
    }
}

package w5ex1;

public abstract class Fruit {

    protected String name;
    protected float price;
    protected String date;
    protected int ammount;
    protected String original;

    public Fruit() {
    }

    public Fruit(String name, float price, String date, int ammount, String original) {
        this.name = name;
        this.price = price;
        this.date = date;
        this.ammount = ammount;
        this.original = original;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getAmmount() {
        return ammount;
    }

    public void setAmmount(int ammount) {
        this.ammount = ammount;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    @Override
    public String toString() {
        return String.format("%s : %.2fvnd/kg - %s - %dkg - %s", name, price, date, ammount, original);
    }
}

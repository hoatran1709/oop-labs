package w5ex1;

public class Orange extends Fruit {
    protected String type;

    public Orange() {
    }

    public Orange(String type) {
        this.type = type;
    }

    public Orange(float price, String date, int ammount, String original, String type) {
        super("Orange", price, date, ammount, original);
        this.type = type;

        System.out.println(toString());
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return super.toString() + " - " + type;
    }
}

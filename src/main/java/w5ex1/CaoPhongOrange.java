package w5ex1;

public class CaoPhongOrange extends Orange {

    public CaoPhongOrange() {
        super("Cao Phong");
    }

    public CaoPhongOrange(float price, String date, int ammount, String original) {
        super(price, date, ammount, original, "Cao Phong");
    }
}

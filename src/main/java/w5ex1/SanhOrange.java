package w5ex1;

public class SanhOrange extends Orange {

    public SanhOrange() {
        super("Sanh");
    }

    public SanhOrange(float price, String date, int ammount, String original) {
        super(price, date, ammount, original, "Sanh");
    }
}

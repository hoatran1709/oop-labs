package w5ex1;

/**
 * Main class for week 5 exercise 1
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class Main {

    /**
     * Main function
     *
     * @param args list parameters
     */
    public static void main(String[] args) {
        Orange orange = new Orange(15000, "10/12/2018", 10, "Vietnam", "Origin");
        Orange caoPhongOrange = new CaoPhongOrange(25000, "15/12/2018", 20, "Vietnam");
        Orange sanhOrange = new SanhOrange(20000, "9/12/2018", 15, "Vietnam");

        Apple apple = new Apple(21000, "20/12/2018", 9, "Vietnam");
    }
}

package w5ex1;

public class Apple extends Fruit {

    public Apple() {
    }

    public Apple(float price, String date, int ammount, String original) {
        super("Apple", price, date, ammount, original);

        System.out.println(toString());
    }
}

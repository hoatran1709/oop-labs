package w4;

/**
 * JUnitMain - Main class for week 4
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class JUnitMain {

    /**
     * Find max of two integer
     *
     * @param a value one
     * @param b value two
     * @return max of two integer
     */
    public static int maxOfTwoInteger(int a, int b) {
        if (a > b) return a;
        else return b;
    }

    /**
     * Find max of an array
     *
     * @param arr input array
     * @return max of input array
     */
    public static int maxOfAnArray(int[] arr) {
        int max = arr[0];

        for (int i = 0; i < arr.length; ++i) {
            if (arr[i] > max) max = arr[i];
        }

        return max;
    }

    /**
     * Calculate BMI
     *
     * @param w weight
     * @param h height
     * @return bmi
     */
    public static float calculateBMI(float w, float h) {
        return w / (h * h);
    }

    /**
     * Get information from bmi
     *
     * @param bmi input bmi
     * @return information
     */
    public static String parseBMItoInfo(float bmi) {
        if (bmi < 18.5) return "Thieu can";
        else if (bmi < 22.99) return "Binh thuong";
        else if (bmi < 24.99) return "Thua can";
        else return "Beo phi";
    }

    /**
     * Main function
     *
     * @param args list parameters
     */
    public static void main(String[] args) {
        System.out.println(maxOfTwoInteger(8, 2));
        System.out.println(maxOfAnArray(new int[] {10, 9, -1, -199, 9, 0}));
        System.out.println(parseBMItoInfo(calculateBMI(55, 1.85f)));
    }
}

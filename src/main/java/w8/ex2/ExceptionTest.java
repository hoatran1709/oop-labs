package w8.ex2;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * ExceptionTest - Main class for week 8 exercise 2
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class ExceptionTest {
    /**
     * For fast test
     */
    public interface Maker {
        public void make() throws Exception;
    }

    /**
     * Make NullPointerException
     *
     * @throws NullPointerException
     */
    public static void MakeNullPointerException() throws NullPointerException {
        throw new NullPointerException();
    }

    /**
     * Make ArrayIndexOutOfBoundsException
     *
     * @throws ArrayIndexOutOfBoundsException
     */
    public static void MakeArrayIndexOutOfBoundsException() throws ArrayIndexOutOfBoundsException {
        throw new ArrayIndexOutOfBoundsException();
    }

    /**
     * Make ArithmeticException
     *
     * @throws ArithmeticException
     */
    public static void MakeArithmeticException() throws ArithmeticException {
        throw new ArithmeticException();
    }

    /**
     * Make ClassCastException
     *
     * @throws ClassCastException
     */
    public static void MakeClassCastException() throws ClassCastException {
        throw new ClassCastException();
    }

    /**
     * Make IOException
     *
     * @throws IOException
     */
    public static void MakeIOException() throws IOException {
        throw new IOException();
    }

    /**
     * Make FileNotFoundException
     *
     * @throws FileNotFoundException
     */
    public static void MakeFileNotFoundException() throws FileNotFoundException {
        throw new FileNotFoundException();
    }

    /**
     * Function to run and get exception
     *
     * @param maker
     */
    public static void Runner(Maker maker) {
        try {
            maker.make();
        } catch (Exception e) {
            System.out.println("------------------------------------------");
            e.printStackTrace();
            System.out.println("------------------------------------------");
        }
    }

    /**
     * Main function
     *
     * @param args list parameters
     */
    public static void main(String[] args) {
        Runner(ExceptionTest::MakeNullPointerException);
        Runner(ExceptionTest::MakeArrayIndexOutOfBoundsException);
        Runner(ExceptionTest::MakeArithmeticException);
        Runner(ExceptionTest::MakeClassCastException);
        Runner(ExceptionTest::MakeIOException);
        Runner(ExceptionTest::MakeFileNotFoundException);
    }
}
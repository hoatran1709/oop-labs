package w8.ex1;

/**
 * Numeral
 */
public class Numeral extends Expression {
    private int value;

    /**
     * Constructor
     *
     * @param value initial value
     */
    public Numeral(int value) {
        this.value = value;
    }

    /**
     * Numeral information
     */
    public String toString() {
        return String.valueOf(value);
    }

    /**
     * Evaluate numeral
     *
     * @return value of number
     */
    public int evaluate() {
        return value;
    }
}
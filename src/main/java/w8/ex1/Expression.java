package w8.ex1;

/**
 * Expression
 */
public abstract class Expression {
    public abstract String toString();

    public abstract int evaluate();
}
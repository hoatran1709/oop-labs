package w8.ex1;

/**
 * Multiplication
 */
public class Multiplication extends BinaryExpression {
    /**
     * Constructor
     *
     * @param left  initial left expression
     * @param right initial right expression
     */
    public Multiplication(Expression left, Expression right) {
        super(left, right);
    }

    /**
     * Multiplication information
     */
    public String toString() {
        return super.left.toString() + " * " + super.right.toString();
    }

    /**
     * Evaluate multiplication
     *
     * @return value of multiplication
     */
    public int evaluate() {
        return super.left.evaluate() * super.right.evaluate();
    }
}
package w8.ex1;

/**
 * ExpressionTest - Main class for week 8 exercise 1
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class ExpressionTest {

    /**
     * Main function
     *
     * @param args list parameters
     */
    public static void main(String[] args) {
        Expression expression = new Square(new Addition(new Subtraction(new Square(new Numeral(10)), new Numeral(1)),
                new Multiplication(new Numeral(2), new Numeral(3))));
        System.out.println(expression.toString() + " = " + expression.evaluate());

        try {
            expression = new Division(new Numeral(2), new Numeral(0));

            System.out.println(expression.toString() + " = " + expression.evaluate());
        } catch (ArithmeticException db0) {
            db0.printStackTrace();
            System.out.println("Caught exception!");
        }
    }
}
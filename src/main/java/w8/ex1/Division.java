package w8.ex1;

/**
 * Division
 */
public class Division extends BinaryExpression {
    /**
     * Constructor
     *
     * @param left  initial left expression
     * @param right initial right expression
     */
    public Division(Expression left, Expression right) {
        super(left, right);
    }

    /**
     * Division information
     */
    public String toString() {
        return super.left.toString() + " / " + super.right.toString();
    }

    /**
     * Evaluate division
     *
     * @return value of division
     */
    public int evaluate() {
        return super.left.evaluate() / super.right.evaluate();
    }
}
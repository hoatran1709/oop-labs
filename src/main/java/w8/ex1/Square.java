package w8.ex1;

/**
 * Square
 */
public class Square extends Expression {
    Expression expression;

    /**
     * Constructor
     *
     * @param expression initial expression
     */
    public Square(Expression expression) {
        this.expression = expression;
    }

    /**
     * Square information
     */
    public String toString() {
        return "(" + expression.toString() + ")^2";
    }

    /**
     * Evaluate square
     */
    public int evaluate() {
        return (int) Math.pow(expression.evaluate(), 2);
    }
}
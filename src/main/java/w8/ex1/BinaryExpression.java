package w8.ex1;

/**
 * BinaryExpression
 */
public abstract class BinaryExpression extends Expression {
    protected Expression left, right;

    public BinaryExpression(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }
}
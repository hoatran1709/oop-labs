package w8.ex1;

/**
 * Addition
 */
public class Addition extends BinaryExpression {
    /**
     * Constructor
     *
     * @param left  initial left expression
     * @param right initial right expression
     */
    public Addition(Expression left, Expression right) {
        super(left, right);
    }

    /**
     * Addition information
     */
    public String toString() {
        return super.left.toString() + " + " + super.right.toString();
    }

    /**
     * Evaluate addition
     *
     * @return value of addition
     */
    public int evaluate() {
        return super.left.evaluate() + super.right.evaluate();
    }
}
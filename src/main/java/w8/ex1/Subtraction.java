package w8.ex1;

/**
 * Subtraction
 */
public class Subtraction extends BinaryExpression {
    /**
     * Constructor
     *
     * @param left  initial left expression
     * @param right initial right expression
     */
    public Subtraction(Expression left, Expression right) {
        super(left, right);
    }

    /**
     * Subtraction information
     */
    public String toString() {
        return super.left.toString() + " - " + super.right.toString();
    }

    /**
     * Evaluate subtraction
     *
     * @return value of subtraction
     */
    public int evaluate() {
        return super.left.evaluate() - super.right.evaluate();
    }
}
package w2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * StudentManagement - Main class for week 2
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class StudentManagement {
    private static List<Student> students = new ArrayList<>(100);

    /**
     * Check if two students are in same group
     *
     * @param s1 Student one
     * @param s2 Student two
     * @return same group?
     */
    private static boolean sameGroup(Student s1, Student s2) {
        return s1.getGroup().equalsIgnoreCase(s2.getGroup());
    }

    /**
     * Print all students in same groups
     */
    private static void studentByGroup() {
        List<Student> temp = new ArrayList<>(students);

        temp.sort(Comparator.comparing(Student::getGroup));

        for (Student student : temp) student.getInfo();
    }

    /**
     * Remove a student with id
     *
     * @param id id of student to remove
     */
    private static void removeStudent(String id) {
        for (int i = 0; i < students.size(); ++i)
            if (students.get(i).getId().equalsIgnoreCase(id)) students.remove(i);
    }

    /**
     * Main function
     *
     * @param args list parameters
     */
    public static void main(String[] args) {
        Student nullStudent = new Student();
        Student myself = new Student("Tran Ba Hoa", "17021251", "K62CACLC1", "vietnamvodichhsts@gmail.com");
        Student myself2 = new Student(myself);

        Student studentOne = new Student("Somebody", "SomeID", "K59CB", "SomeEmail@mail");
        Student studentTwo = new Student(studentOne);
        Student studentThree = new Student("Somebody", "SomeID", "K59CLC", "SomeEmail@mail");

        nullStudent.getInfo();
        myself.getInfo();
        myself2.getInfo();

        System.out.println(sameGroup(studentOne, studentTwo));
        System.out.println(sameGroup(studentThree, studentTwo));

        System.out.println("----------------------------------------------");

        students.add(nullStudent);
        students.add(myself);
        students.add(myself2);
        students.add(studentOne);
        students.add(studentTwo);
        students.add(studentThree);

        studentByGroup();

        removeStudent("null");

        System.out.println("----------------------------------------------");
        studentByGroup();
    }
}
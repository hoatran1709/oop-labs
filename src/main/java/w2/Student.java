package w2;

/**
 * Student
 */
public class Student {
    private String name;
    private String id;
    private String group;
    private String email;

    /**
     * Default constructor
     */
    public Student() {
        name = id = group = email = "null";
    }

    /**
     * Parameters constructor
     *
     * @param name  name of student
     * @param id    id of student
     * @param group group of student
     * @param email email of student
     */
    public Student(String name, String id, String group, String email) {
        this.name = name;
        this.id = id;
        this.group = group;
        this.email = email;
    }

    /**
     * Copy constructor
     *
     * @param other other Student to get data into this Student
     */
    public Student(Student other) {
        this.name = other.name;
        this.id = other.id;
        this.group = other.group;
        this.email = other.email;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the group
     */
    public String getGroup() {
        return group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(String group) {
        this.group = group;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Print information of this student
     */
    public void getInfo() {
        System.out.println(toString());
    }

    /**
     * Override toString function
     *
     * @return infomation of student
     */
    @Override
    public String toString() {
        return name + ": " + id + " - " + group + " - " + email;
    }
}
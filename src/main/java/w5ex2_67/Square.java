package w5ex2_67;

/**
 * Square shape
 */
public class Square extends Rectangle {
    /**
     * Default constructor
     */
    public Square() {
        super();
    }

    /**
     * Constructor with given width height
     *
     * @param s initial side
     */
    public Square(int s) {
        super(s, s);
    }

    /**
     * Construcor with given information
     *
     * @param posx     initial pos X
     * @param posy     initial pos Y
     * @param moveable initial is moveable
     * @param filled   initial is filled
     * @param s        initial side
     */
    public Square(int posx, int posy, boolean moveable, boolean filled, int s) {
        super(posx, posy, moveable, filled, s, s);
    }

    /**
     * Equal to other
     *
     * @param other other square
     * @return if two square are equal
     */
    @Override
    public boolean equals(Object other) {
        return (super.equals(other) && (other instanceof Square));
    }

    /**
     * To string
     *
     * @return infomation of square
     */
    public String toString() {
        return super.toString() + " " + w;
    }
}
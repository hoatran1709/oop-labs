package w5ex2_67;

/**
 * Triangle shape
 */
public class Triangle extends Shape {
    private int f, s, t;

    /**
     * Default constructor
     */
    public Triangle() {
        super();
        f = s = t = 0;
    }

    /**
     * Constructor with given 3 lines of triangle
     *
     * @param f initial first line
     * @param s initial second line
     * @param t initial third line
     */
    public Triangle(int f, int s, int t) {
        this();
        this.f = f;
        this.s = s;
        this.t = t;
    }

    /**
     * Construcor with given information
     *
     * @param posx     initial pos X
     * @param posy     initial pos Y
     * @param moveable initial is moveable
     * @param filled   initial is filled
     * @param s        initial side
     */
    public Triangle(int posx, int posy, boolean moveable, boolean filled, int f, int s, int t) {
        super(posx, posy, moveable, filled);
        setF(f);
        setS(s);
        setT(t);
    }

    /**
     * @return the f
     */
    public int getF() {
        return f;
    }

    /**
     * @param f the f to set
     */
    public void setF(int f) {
        this.f = f;
    }

    /**
     * @return the s
     */
    public int getS() {
        return s;
    }

    /**
     * @param s the s to set
     */
    public void setS(int s) {
        this.s = s;
    }

    /**
     * @return the t
     */
    public int getT() {
        return t;
    }

    /**
     * @param t the t to set
     */
    public void setT(int t) {
        this.t = t;
    }

    /**
     * Equal to other
     *
     * @param other other hexagon
     * @return if two hexagon are equal
     */
    @Override
    public boolean equals(Object other) {
        return (super.equals(other) && (other instanceof Triangle) && this.f == ((Triangle) other).f && this.s == ((Triangle) other).s && this.t == ((Triangle) other).t);
    }

    /**
     * To string
     *
     * @return infomation of triangle
     */
    public String toString() {
        return super.toString() + " " + f + " " + s + " " + t;
    }
}
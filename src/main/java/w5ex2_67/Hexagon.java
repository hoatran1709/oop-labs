package w5ex2_67;

/**
 * Hexagon shape
 */
public class Hexagon extends Shape {
    private int s;

    /**
     * Default constructor
     */
    public Hexagon() {
        super();
        s = 0;
    }

    /**
     * Constructor with given side
     *
     * @param s initial side
     */
    public Hexagon(int s) {
        this();
        this.s = s;
    }

    /**
     * Construcor with given information
     *
     * @param posx     initial pos X
     * @param posy     initial pos Y
     * @param moveable initial is moveable
     * @param filled   initial is filled
     * @param s        initial side
     */
    public Hexagon(int posx, int posy, boolean moveable, boolean filled, int s) {
        super(posx, posy, moveable, filled);
        setS(s);
    }

    /**
     * @return the s
     */
    public int getS() {
        return s;
    }

    /**
     * @param s the s to set
     */
    public void setS(int s) {
        this.s = s;
    }

    /**
     * Equal to other
     *
     * @param other other hexagon
     * @return if two hexagon are equal
     */
    @Override
    public boolean equals(Object other) {
        return (super.equals(other) && (other instanceof Hexagon) && this.s == ((Hexagon) other).s);
    }

    /**
     * To string
     *
     * @return infomation of hexagon
     */
    public String toString() {
        return super.toString() + " " + s;
    }
}
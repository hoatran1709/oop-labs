package w5ex2_67;

/**
 * Shape class
 */
public abstract class Shape extends Object {
    protected int pos_x, pos_y;
    protected boolean isMovable;
    protected boolean isFilled;

    /**
     * Default constructor
     */
    public Shape() {
        pos_x = pos_y = 0;
        isFilled = isMovable = false;
    }

    /**
     * Construcor with given information
     *
     * @param posx     initial pos X
     * @param posy     initial pos Y
     * @param moveable initial is moveable
     * @param filled   initial is filled
     */
    public Shape(int posx, int posy, boolean moveable, boolean filled) {
        setPos_x(posx);
        setPos_y(posy);
        setIsMovable(moveable);
        setIsFilled(filled);
    }

    /**
     * @return the pos_x
     */
    public int getPos_x() {
        return pos_x;
    }

    /**
     * @param pos_x the pos_x to set
     */
    public void setPos_x(int pos_x) {
        this.pos_x = pos_x;
    }

    /**
     * @return the pos_y
     */
    public int getPos_y() {
        return pos_y;
    }

    /**
     * @param pos_y the pos_y to set
     */
    public void setPos_y(int pos_y) {
        this.pos_y = pos_y;
    }

    /**
     * @return the isFilled
     */
    public boolean isFilled() {
        return isFilled;
    }

    /**
     * @param isFilled the isFilled to set
     */
    public void setIsFilled(boolean isFilled) {
        this.isFilled = isFilled;
    }

    /**
     * @return the isMovable
     */
    public boolean isMovable() {
        return isMovable;
    }

    /**
     * @param isMovable the isMovable to set
     */
    public void setIsMovable(boolean isMovable) {
        this.isMovable = isMovable;
    }

    /**
     * Equal to other
     *
     * @param other other shape
     * @return if two shape are equal
     */
    @Override
    public boolean equals(Object other) {
        return ((other instanceof Shape) && this.pos_x == ((Shape) other).pos_x && this.pos_y == ((Shape) other).pos_y && this.isFilled == ((Shape) other).isFilled
                && this.isMovable == ((Shape) other).isMovable);
    }

    /**
     * Information of shape
     */
    public String toString() {
        return this.getClass().getCanonicalName() + " : " + pos_x + " " + pos_y + " " + isFilled + " " + isMovable;
    }
}
package w5ex2_67;

/**
 * Circle shape
 */
public class Circle extends Shape {
    private int r;

    /**
     * Default constructor
     */
    public Circle() {
        super();
        r = 0;
    }

    /**
     * Constructor with given radius
     *
     * @param r initial radius
     */
    public Circle(int r) {
        this();
        this.r = r;
    }

    /**
     * Construcor with given information
     *
     * @param posx     initial pos X
     * @param posy     initial pos Y
     * @param moveable initial is moveable
     * @param filled   initial is filled
     * @param r        initial radius
     */
    public Circle(int posx, int posy, boolean moveable, boolean filled, int r) {
        super(posx, posy, moveable, filled);
        setR(r);
    }

    /**
     * @return the r
     */
    public int getR() {
        return r;
    }

    /**
     * @param r the r to set
     */
    public void setR(int r) {
        this.r = r;
    }

    /**
     * Equal to other
     *
     * @param other other circle
     * @return if two circle are equal
     */
    @Override
    public boolean equals(Object other) {
        return (super.equals(other) && (other instanceof Circle) && this.r == ((Circle) other).r);
    }

    /**
     * To string
     *
     * @return infomation of circle
     */
    public String toString() {
        return super.toString() + " " + r;
    }
}
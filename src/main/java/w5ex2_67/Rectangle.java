package w5ex2_67;

/**
 * Rectangle shape
 */
public class Rectangle extends Shape {
    protected int w, h;

    /**
     * Default constructor
     */
    public Rectangle() {
        super();
        w = h = 0;
    }

    /**
     * Constructor with given width height
     *
     * @param w initial width
     * @param h initial height
     */
    public Rectangle(int w, int h) {
        this();
        this.w = w;
        this.h = h;
    }

    /**
     * Construcor with given information
     *
     * @param posx     initial pos X
     * @param posy     initial pos Y
     * @param moveable initial is moveable
     * @param filled   initial is filled
     * @param w        initial width
     * @param h        initial height
     */
    public Rectangle(int posx, int posy, boolean moveable, boolean filled, int w, int h) {
        super(posx, posy, moveable, filled);
        setW(w);
        setH(h);
    }

    /**
     * @return the h
     */
    public int getH() {
        return h;
    }

    /**
     * @param h the h to set
     */
    public void setH(int h) {
        this.h = h;
    }

    /**
     * @return the w
     */
    public int getW() {
        return w;
    }

    /**
     * @param w the w to set
     */
    public void setW(int w) {
        this.w = w;
    }

    /**
     * Equal to other
     *
     * @param other other rectangle
     * @return if two rectangle are equal
     */
    @Override
    public boolean equals(Object other) {
        return (super.equals(other) && (other instanceof Rectangle) && this.w == ((Rectangle) other).w && this.h == ((Rectangle) other).h);
    }

    /**
     * To string
     *
     * @return infomation of rectangle
     */
    public String toString() {
        return super.toString() + " " + w + " " + h;
    }
}
package w5ex2_67;

import java.util.Vector;

/**
 * Layer
 */
public class Layer {
    private Vector<Shape> shapes;
    private boolean isVisible;

    /**
     * Default constructor
     */
    public Layer() {
        shapes = new Vector<Shape>();
        isVisible = true;
    }

    /**
     * Constructor with given visible status
     *
     * @param visible initial visible
     */
    public Layer(boolean visible) {
        this();
        this.isVisible = visible;
    }

    /**
     * Add shape
     *
     * @param shape new shape
     */
    public void addShape(Shape shape) {
        shapes.add(shape);
    }

    /**
     * Get shape
     *
     * @param index index in list
     * @return the shape
     */
    public Shape getShape(int index) {
        return shapes.get(index);
    }

    /**
     * @return the shapes
     */
    public Vector<Shape> getShapes() {
        return shapes;
    }

    /**
     * @param shapes the shapes to set
     */
    public void setShapes(Vector<Shape> shapes) {
        this.shapes = shapes;
    }

    /**
     * @return the isVisible
     */
    public boolean isVisible() {
        return isVisible;
    }

    /**
     * @param isVisible the isVisible to set
     */
    public void setIsVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    /**
     * Layer information
     */
    public String toString() {
        StringBuilder res = new StringBuilder("Layer : ");

        for (Shape shape : shapes)
            res.append("\n\t\t" + shape.toString());

        return res.toString();
    }

    /**
     * Delete triangle week 6
     */
    public void deleteTriangle() {
        for (int i = shapes.size() - 1; i >= 0; --i)
            if (shapes.get(i) instanceof Rectangle)
                shapes.remove(shapes.get(i));
    }

    /**
     * Delete all same shape week 7
     */
    public void filterShape() {
        Vector<Shape> temp = new Vector<>();

        for (Shape shape : shapes) {
            if (temp.indexOf(shape) < 0) temp.add(shape);
        }

        shapes = temp;
    }
}
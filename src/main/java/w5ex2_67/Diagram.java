package w5ex2_67;

import java.util.Vector;

/**
 * Diagram
 */
public class Diagram {
    private Vector<Layer> layers;

    /**
     * Default constructor
     */
    public Diagram() {
        layers = new Vector<>();
    }

    /**
     * Add layer
     *
     * @param layer new layer
     */
    public void addLayer(Layer layer) {
        layers.add(layer);
    }

    /**
     * Get layer
     *
     * @param index index in list
     * @return the layer
     */
    public Layer getLayer(int index) {
        return layers.get(index);
    }

    /**
     * @return the layers
     */
    public Vector<Layer> getLayers() {
        return layers;
    }

    /**
     * @param layers the layers to set
     */
    public void setLayers(Vector<Layer> layers) {
        this.layers = layers;
    }

    /**
     * Diagram information
     */
    public String toString() {
        StringBuilder res = new StringBuilder("----------------------------------------------------\nDiagram : ");

        for (Layer layer : layers)
            if (layer.isVisible())
                res.append("\n\t" + layer.toString());

        res.append("\n----------------------------------------------------\n");

        return res.toString();
    }

    /**
     * Delete circle week 6
     */
    public void deleteCircle() {
        for (Layer layer : layers)
            for (int i = layer.getShapes().size() - 1; i >= 0; --i)
                if (layer.getShapes().get(i) instanceof Circle)
                    layer.getShapes().remove(layer.getShapes().get(i));
    }

    /**
     * Filter layer week 7
     */
    public void filterLayer() {
        Diagram temp = new Diagram();

        for (Layer layer : layers) {
            for (Shape shape : layer.getShapes()) {
                int index = temp.findLayerStartByClass(shape.getClass());

                if (index == -1) {
                    temp.addLayer(new Layer());
                    temp.getLayer(temp.getLayers().size() - 1).addShape(shape);
                } else {
                    temp.getLayer(index).addShape(shape);
                }
            }
        }

        this.layers = temp.layers;
    }

    /**
     * Find layer in list start with a class type
     *
     * @param classInput class input to check
     * @return index of the layer if found, and -1 if not found
     */
    private int findLayerStartByClass(Class classInput) {
        for (int i = 0; i < layers.size(); ++i)
            if (getLayerClassHolder(i).getCanonicalName().equalsIgnoreCase(classInput.getCanonicalName()))
                return i;

        return -1;
    }

    /**
     * Get the class of first shape in a layer
     *
     * @param index index of layer
     * @return the class of first shape in that layer
     */
    private Class getLayerClassHolder(int index) {
        try {
            return getLayer(index).getShapes().get(0).getClass();
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }
}
package w5ex2_67;

/**
 * Main class for Week 5 exercise 2 and week 6 and 7
 *
 * @author Tran Ba Hoa - K62 CACLC1
 * @version last version
 */
public class Main {
    public static Diagram diagram = new Diagram();

    /**
     * Main function
     *
     * @param args list parameters
     */
    public static void main(String[] args) {
        diagram.addLayer(new Layer(true));
        diagram.addLayer(new Layer(true));
        diagram.addLayer(new Layer(true));

        Layer layer1 = diagram.getLayer(0);
        layer1.addShape(new Circle(50, 5, true, true, 10));
        layer1.addShape(new Circle(50, 5, true, true, 10));
        layer1.addShape(new Circle(50, 5, true, false, 100));
        layer1.addShape(new Circle(50, 5, true, true, 10));
        layer1.addShape(new Circle(50, 5, true, false, 100));
        layer1.addShape(new Circle(50, 5, true, true, 1));

        Layer layer2 = diagram.getLayer(1);
        layer2.addShape(new Hexagon(-100, 9, false, true, 100));
        layer2.addShape(new Triangle(500, -10, true, false, 10, 10, 10));
        layer2.addShape(new Circle(500, 5, false, false, 101));
        layer2.addShape(new Rectangle(90, 5, true, true, 1000, 90));
        layer2.addShape(new Hexagon(-100, 9, false, true, 100));

        Layer layer3 = diagram.getLayer(2);
        layer3.addShape(new Circle(-100, 9, false, true, 100));
        layer3.addShape(new Triangle(500, -10, true, false, 10, 10, 10));
        layer3.addShape(new Rectangle(90, 5, true, true, 1000, 90));
        layer3.addShape(new Triangle(-100, 9, false, true, 100, 10, 10));
        layer3.addShape(new Circle(-100, 9, false, true, 100));

        layer1.filterShape();
        layer2.filterShape();
        layer3.filterShape();

        System.out.println(diagram.toString());

        diagram.filterLayer();

        System.out.println(diagram.toString());
    }
}
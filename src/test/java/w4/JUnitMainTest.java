package w4;

import org.junit.Test;

import static org.junit.Assert.*;

public class JUnitMainTest {

    @Test
    public void maxOfTwoInteger() {
        assertEquals(8, JUnitMain.maxOfTwoInteger(8, 3));
        assertEquals(-100, JUnitMain.maxOfTwoInteger(-100, -199));
        assertEquals(100, JUnitMain.maxOfTwoInteger(80, 100));
        assertEquals(0, JUnitMain.maxOfTwoInteger(0, -1));
        assertEquals(1000, JUnitMain.maxOfTwoInteger(-1000, 1000));
    }

    @Test
    public void maxOfAnArray() {
        assertEquals(10, JUnitMain.maxOfAnArray(new int[] {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
        assertEquals(-100, JUnitMain.maxOfAnArray(new int[] {-1000, -1999, -1922, -100, -1002, -101}));
        assertEquals(250, JUnitMain.maxOfAnArray(new int[] {0, 9, 210, 250, 40, 51, 64, 76, -112, 21, 10}));
        assertEquals(5235, JUnitMain.maxOfAnArray(new int[] {230, -11, 2322, 33, 44, 5235, 126, 457, 68, -19, -10}));
        assertEquals(1000, JUnitMain.maxOfAnArray(new int[] {1000, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
    }

    @Test
    public void parseBMItoInfo() {
        assertEquals("Binh thuong", JUnitMain.parseBMItoInfo(JUnitMain.calculateBMI(65, 1.8f)));
        assertEquals("Beo phi", JUnitMain.parseBMItoInfo(JUnitMain.calculateBMI(70, 1.6f)));
        assertEquals("Thua can", JUnitMain.parseBMItoInfo(JUnitMain.calculateBMI(80, 1.85f)));
        assertEquals("Thieu can", JUnitMain.parseBMItoInfo(JUnitMain.calculateBMI(55, 1.85f)));
        assertEquals("Binh thuong", JUnitMain.parseBMItoInfo(JUnitMain.calculateBMI(70, 1.85f)));
    }
}